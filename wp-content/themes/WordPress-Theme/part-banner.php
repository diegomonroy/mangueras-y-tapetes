<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'nosotros' ) ) ) : dynamic_sidebar( 'banner_nosotros' ); endif; ?>
				<?php if ( is_page( array( 'productos' ) ) ) : dynamic_sidebar( 'banner_productos' ); endif; ?>
				<?php if ( is_page( array( 'contactanos' ) ) ) : dynamic_sidebar( 'banner_contactanos' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->